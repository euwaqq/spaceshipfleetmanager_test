package itis.quiz.spaceships;

import java.util.ArrayList;

public class SpaceshipFleetManager_Test {

    static SpaceshipFleetManager_Test test = new SpaceshipFleetManager_Test();
    CommandCenter center = new CommandCenter();
    static ArrayList<Spaceship> spaceships =new ArrayList<>();

    public static void main(String[] args) {
        int bulls=0;
        if(test.getMostPowerfulShip_Test()){
            bulls++;
        }
        if(test.getShipByName_Test()){
            bulls++;
        }
        if(test.getAllShipsWithEnoughCargoSpace_Test()){
            bulls++;
        }
        if(test.getAllCivilianShips_Test()){
            bulls++;
        }
        System.out.println("Код набрал "+bulls+" балла из 4-х");
    }


    public boolean getMostPowerfulShip_Test(){
        return (getMostPowerfulShip_Test_ReturnMPS())&&getMostPowerfulShip_Test_ReturnFirst()&&getMostPowerfulShip_Test_ReturnNull();
    }
    public boolean getMostPowerfulShip_Test_ReturnMPS(){
        spaceships.clear();
        spaceships.add(new Spaceship("Aurora",3,6,10));
        spaceships.add(new Spaceship("Bishop",5,3,7));
        Spaceship ship = center.getMostPowerfulShip(spaceships);
        return ship.getName().equals("Bishop")&&ship.getFirePower()==5;
    }
    public boolean getMostPowerfulShip_Test_ReturnFirst(){
        spaceships.clear();
        spaceships.add(new Spaceship("Aurora",5,6,10));
        spaceships.add(new Spaceship("Bishop",5,3,7));
        Spaceship ship = center.getMostPowerfulShip(spaceships);
        return ship.getName().equals("Aurora");
    }
    public boolean getMostPowerfulShip_Test_ReturnNull(){
        spaceships.clear();
        spaceships.add(new Spaceship("Aurora",0,6,10));
        spaceships.add(new Spaceship("Bishop",0,3,7));
        Spaceship ship = center.getMostPowerfulShip(spaceships);
        return ship == null;
    }


    public boolean getShipByName_Test(){
        return getShipByName_Test_returnShip()&&getShipByName_Test_returnNull();
    }
    boolean getShipByName_Test_returnShip(){
        spaceships.clear();
        spaceships.add(new Spaceship("Aurora",3,6,10));
        spaceships.add(new Spaceship("Bishop",5,3,7));
        Spaceship ship = center.getShipByName(spaceships,"Bishop");
        return ship.getName().equals("Bishop");
    }
    boolean getShipByName_Test_returnNull(){
        spaceships.clear();
        spaceships.add(new Spaceship("Aurora",3,6,10));
        spaceships.add(new Spaceship("Bishop",5,3,7));
        Spaceship ship = center.getShipByName(spaceships,"Bzya");
        return ship == null;
    }


    boolean getAllShipsWithEnoughCargoSpace_Test(){
        return getAllShipsWithEnoughCargoSpace_Test_returnShips()&&getAllShipsWithEnoughCargoSpace_Test_returnNull();
    }
    boolean getAllShipsWithEnoughCargoSpace_Test_returnShips(){
        spaceships.clear();
        spaceships.add(new Spaceship("Aurora",3,6,10));
        spaceships.add(new Spaceship("Bzya",1,0,1));
        spaceships.add(new Spaceship("Bishop",5,3,7));
        ArrayList<Spaceship> result=center.getAllShipsWithEnoughCargoSpace(spaceships,3);
        for(int i=0;i<result.size();i++) {
            if(result.get(i).getCargoSpace()<3){
                return false;
            }
        }
        return true;
    }
    boolean getAllShipsWithEnoughCargoSpace_Test_returnNull() {
        spaceships.clear();
        spaceships.add(new Spaceship("Aurora", 3, 6, 10));
        spaceships.add(new Spaceship("Bzya", 1, 0, 1));
        spaceships.add(new Spaceship("Bishop", 5, 3, 7));
        return center.getAllShipsWithEnoughCargoSpace(spaceships, 7).size()==0;
    }



    boolean getAllCivilianShips_Test(){
        return getAllCivilianShips_Test_returnShips()&&getAllCivilianShips_Test_returnNull();
    }
    boolean getAllCivilianShips_Test_returnShips(){
        spaceships.clear();
        spaceships.add(new Spaceship("Aurora",-3,6,10));
        spaceships.add(new Spaceship("Bzya",1,0,1));
        spaceships.add(new Spaceship("Bishop",0,3,7));
        ArrayList<Spaceship> result=center.getAllCivilianShips(spaceships);
        for(int i=0;i<result.size();i++) {
            if (result.get(i).getFirePower() > 0) {
                return false;
            }
        }
        return true;
    }
    boolean getAllCivilianShips_Test_returnNull(){
        spaceships.clear();
        spaceships.add(new Spaceship("Aurora", 3, 6, 10));
        spaceships.add(new Spaceship("Bzya", 1, 0, 1));
        spaceships.add(new Spaceship("Bishop", 5, 3, 7));
        return center.getAllCivilianShips(spaceships).size()==0;
    }
}
