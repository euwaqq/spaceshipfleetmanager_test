package itis.quiz.spaceships;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;

public class SpaceshipFleetManager_JunitTest {
        static CommandCenter center = new CommandCenter();
        static ArrayList<Spaceship> spaceships = new ArrayList<>();

    @AfterEach
    void afterEach() {
        spaceships.clear();
    }
    @Test
    void getMostPowerfulShip_Test_ReturnMPS(){
        spaceships.add(new Spaceship("Aurora",3,6,10));
        spaceships.add(new Spaceship("Bishop",5,3,7));
        Assertions.assertEquals(center.getMostPowerfulShip(spaceships),spaceships.get(1));
    }
    @Test
    void getMostPowerfulShip_Test_ReturnFirst(){
        spaceships.add(new Spaceship("Aurora",5,6,10));
        spaceships.add(new Spaceship("Bishop",5,3,7));
        Assertions.assertEquals(center.getMostPowerfulShip(spaceships),spaceships.get(0));
    }
    @Test
    void getMostPowerfulShip_Test_ReturnNull(){
        spaceships.add(new Spaceship("Aurora",0,6,10));
        spaceships.add(new Spaceship("Bishop",0,3,7));
        Assertions.assertNull(center.getMostPowerfulShip(spaceships));
    }
    @Test
    void getShipByName_Test_returnShip(){
        spaceships.add(new Spaceship("Aurora",3,6,10));
        spaceships.add(new Spaceship("Bishop",5,3,7));
        Assertions.assertEquals(center.getShipByName(spaceships,"Bishop"),spaceships.get(1));
    }
    @Test
    void getShipByName_Test_returnNull(){
        spaceships.add(new Spaceship("Aurora",3,6,10));
        spaceships.add(new Spaceship("Bishop",5,3,7));
        Assertions.assertNull(center.getShipByName(spaceships,"Bzya"));
    }
    @Test
    void getAllShipsWithEnoughCargoSpace_Test_returnShips(){
        spaceships.add(new Spaceship("Aurora",3,6,10));
        spaceships.add(new Spaceship("Bzya",1,0,1));
        spaceships.add(new Spaceship("Bishop",5,3,7));
        ArrayList<Spaceship> ships = new ArrayList<>();
        ships.add(spaceships.get(0));
        ships.add(spaceships.get(2));
        ArrayList<Spaceship> result=center.getAllShipsWithEnoughCargoSpace(spaceships,3);
        Assertions.assertEquals(result.get(0),ships.get(0));
        Assertions.assertEquals(result.get(1),ships.get(1));
        Assertions.assertEquals(result.size(),2);
    }
    @Test
    void getAllShipsWithEnoughCargoSpace_Test_returnNull(){
        spaceships.add(new Spaceship("Aurora", 3, 6, 10));
        spaceships.add(new Spaceship("Bzya", 1, 0, 1));
        spaceships.add(new Spaceship("Bishop", 5, 3, 7));
        Assertions.assertEquals(center.getAllShipsWithEnoughCargoSpace(spaceships, 7).size(),0);
    }
    @Test
    void getAllCivilianShips_Test_returnShips(){
        spaceships.add(new Spaceship("Aurora",-3,6,10));
        spaceships.add(new Spaceship("Bzya",1,0,1));
        spaceships.add(new Spaceship("Bishop",0,3,7));
        ArrayList<Spaceship> ships = new ArrayList<>();
        ships.add(spaceships.get(0));
        ships.add(spaceships.get(2));
        ArrayList<Spaceship> result=center.getAllCivilianShips(spaceships);
        Assertions.assertEquals(result.get(0),ships.get(0));
        Assertions.assertEquals(result.get(1),ships.get(1));
        Assertions.assertEquals(result.size(),2);
    }
    @Test
    void getAllCivilianShips_Test_returnNull(){
        spaceships.add(new Spaceship("Aurora", 3, 6, 10));
        spaceships.add(new Spaceship("Bzya", 1, 0, 1));
        spaceships.add(new Spaceship("Bishop", 5, 3, 7));
        Assertions.assertEquals(center.getAllCivilianShips(spaceships).size(),0);
    }
}
