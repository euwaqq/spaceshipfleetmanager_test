package itis.quiz.spaceships;


import java.util.ArrayList;

// Вы проектируете интеллектуальную систему управления ангаром командного центра.
// Реализуйте интерфейс SpaceshipFleetManager для управления флотом кораблей.
// Используйте СУЩЕСТВУЮЩИЙ интерфейс и класс космического корабля (SpaceshipFleetManager и Spaceship).
public class CommandCenter implements SpaceshipFleetManager {
    @Override
    public Spaceship getMostPowerfulShip(ArrayList<Spaceship> ships){
       Spaceship mostPowerfulShip= ships.get(0);
       Spaceship testShip;
       for(int i=1;i<ships.size();i++) {
           testShip=ships.get(i);
           if (testShip.getFirePower()>mostPowerfulShip.getFirePower())
               mostPowerfulShip=testShip;
       }
       if (mostPowerfulShip.getFirePower()>0)
           return mostPowerfulShip;
       else
           return null;
    }

    @Override
    public Spaceship getShipByName(ArrayList<Spaceship> ships, String name) {
        for(int i=0;i<ships.size();i++) {
            if(ships.get(i).getName().equals(name))
                return ships.get(i);
        }
        return null;
    }

    @Override
    public ArrayList<Spaceship> getAllShipsWithEnoughCargoSpace(ArrayList<Spaceship> ships, Integer cargoSize) {
        ArrayList<Spaceship> aswecs = new ArrayList<>();
        for(int i=0;i<ships.size();i++) {
            if (ships.get(i).getCargoSpace()>=cargoSize){
                aswecs.add(ships.get(i));
            }
        }
        return aswecs;
    }

    @Override
    public ArrayList<Spaceship> getAllCivilianShips(ArrayList<Spaceship> ships) {
        ArrayList<Spaceship> civils = new ArrayList<>();
        for(int i=0;i<ships.size();i++) {
            if (ships.get(i).getFirePower() <= 0) {
                civils.add(ships.get(i));
            }
        }
        return civils;
    }
}
